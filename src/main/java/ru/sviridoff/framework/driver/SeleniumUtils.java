package ru.sviridoff.framework.driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumUtils {

    public void isPresence(WebDriver driver, By locator, int sec) {
        WebDriverWait wait = new WebDriverWait(driver, sec);
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public boolean isVisible(WebDriver driver, By locator, int sec) {
        boolean flag = false;
        WebDriverWait wait = new WebDriverWait(driver, sec);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        if (driver.findElements(locator).size() != 0) {
            flag = true;
        }
        return flag;
    }

}
