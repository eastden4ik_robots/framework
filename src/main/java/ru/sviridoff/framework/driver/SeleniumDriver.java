package ru.sviridoff.framework.driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import ru.sviridoff.framework.logger.Logger;

import java.util.ArrayList;
import java.util.List;

public class SeleniumDriver {

    private String edgeBinary;
    private String yandexProfile;

    private Logger _log;

    public WebDriver Init(Drivers drivers) {
        _log = new Logger(drivers.name());
        List<String> options = new ArrayList<>();
        options.add("disable-extensions");
        options.add("--start-maximized");
        options.add("--disable-dev-shm-usage"); // overcome limited resource problems
        options.add("--no-sandbox"); // Bypass OS security model
        options.add("--user-data-dir=" + System.getProperty("user.dir") + "/ProfileMem");
        List<String> headlessOptions = new ArrayList<>();
        headlessOptions.add("--headless");
        WebDriver driver;
        if (!drivers.property.equals("") && !drivers.url.equals(""))
            System.setProperty(drivers.property, System.getProperty("user.dir") + drivers.url);
        _log.info(" Driver property: " + drivers.property);
        _log.info(" Driver url: " + drivers.url);
        switch (drivers) {
            case FIREFOX_LINUX:
            case FIREFOX_WINDOWS: {
                driver = new FirefoxDriver(firefoxOptions(options));
                break;
            }
            case YANDEX_WINDOWS: {
                if (yandexProfile != null) {
                    List<String> yandexOptions = new ArrayList<>();
                    yandexOptions.add("--user-data-dir=" + System.getProperty("user.dir") + yandexProfile);
                    driver = new ChromeDriver(chromeOptions(yandexOptions));
                } else
                    driver = new ChromeDriver();
                break;
            }
            case CHROME_LINUX:
            case CHROME_WINDOWS: {
                driver = new ChromeDriver(chromeOptions(options));
                break;
            }
            case EDGE_CHROMIUM: {
                WebDriverManager.edgedriver().setup();
                driver = new EdgeDriver(edgeOptions());
                break;
            }
            case EDGE: {
                driver = new EdgeDriver();
                break;
            }
            case YANDEX_HEADLESS_WIN:
            case CHROME_HEADLESS_WIN: {
                if (yandexProfile != null) {
                    List<String> yandexOptions = new ArrayList<>();
                    yandexOptions.add("--user-data-dir=" + System.getProperty("user.dir") + yandexProfile);
                    yandexOptions.addAll(headlessOptions);
                    driver = new ChromeDriver(chromeOptions(yandexOptions));
                } else
                    driver = new ChromeDriver(chromeOptions(headlessOptions));
                break;
            }
            default: {
                driver = null;
            }
        }
        _log.info(" Driver initialized.");
        return driver;
    }

    public SeleniumDriver setYandexProfile(String profile) {
        this.yandexProfile = profile;
        return this;
    }

    public SeleniumDriver setBinary(String binary) {
        this.edgeBinary = binary;
        return this;
    }

    private EdgeOptions edgeOptions() {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setBinary(edgeBinary);
        return new EdgeOptions().merge(chromeOptions);
    }

    private ChromeOptions chromeOptions(List<String> options) {
        ChromeOptions chromeOptions = new ChromeOptions();
        for (String option: options) {
            chromeOptions.addArguments(option);
        }
        return chromeOptions;
    }

    private FirefoxOptions firefoxOptions(List<String> options) {
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        for (String option: options) {
            firefoxOptions.addArguments(option);
        }
        return firefoxOptions;
    }

    public void Destroy(WebDriver driver) {
        driver.close();
        _log.info(" Driver closed.");
    }

    public enum Drivers {
        CHROME_LINUX("/drivers/chromedriver", "webdriver.chrome.driver"),
        FIREFOX_LINUX("/drivers/geckodriver", "webdriver.gecko.driver"),
        CHROME_HEADLESS_WIN("/drivers/chromedriver.exe", "webdriver.chrome.driver"),
        CHROME_WINDOWS("/drivers/chromedriver.exe", "webdriver.chrome.driver"),
        FIREFOX_WINDOWS("/drivers/geckodriver.exe", "webdriver.gecko.driver"),
        EDGE_CHROMIUM("/drivers/msedgedriver.exe", "webdriver.edge.driver"),
        YANDEX_WINDOWS("/drivers/yandexdriver.exe", "webdriver.chrome.driver"),
        YANDEX_HEADLESS_WIN("/drivers/yandexdriver.exe", "webdriver.chrome.driver"),
        EDGE("", "");

        String url; String property;
        Drivers(String url, String property) {
            this.url = url;
            this.property = property;
        }
    }

}
