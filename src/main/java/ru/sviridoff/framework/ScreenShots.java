package ru.sviridoff.framework;

import ru.sviridoff.framework.logger.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ScreenShots {

    private static Logger logger = new Logger();

    public boolean PrtScr_FullScreen(String fullPath, String ext) {
        boolean result = false;
        try {
            Robot robot = new Robot();
            String fileName = fullPath + "." + ext;

            Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
            BufferedImage screenFullImage = robot.createScreenCapture(screenRect);
            ImageIO.write(screenFullImage, ext, new File(fileName));
            logger.Logging(Logger.LogLevel.SUCCESS, "Full screen saved.");
            result = true;
        } catch (AWTException | IOException ex) {
            logger.Logging(Logger.LogLevel.DANGER, "Error: [" + ex + "].");
        }
        return result;
    }

}
