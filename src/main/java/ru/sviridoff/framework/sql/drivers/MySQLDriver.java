package ru.sviridoff.framework.sql.drivers;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class MySQLDriver {

    @Setter @Getter
    private String user;
    @Setter @Getter
    private String password;
    @Getter
    private String url;
    @Getter
    private final String driverClass = "com.mysql.jdbc.Driver";
    @Getter
    private String database;
    @Getter
    private String hostAddress;
    @Getter
    private int portNum;

    public void setUrl(String host, int port, String dataBase, String tz) {
        hostAddress = host;
        portNum = port;
        database = dataBase;
        url = "jdbc:mysql://" + hostAddress + ":" + portNum + "/" + database + "?serverTimezone=" + tz + "";
    }

}
