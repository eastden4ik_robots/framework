package ru.sviridoff.framework.sql.drivers;

import lombok.Getter;
import lombok.ToString;

@ToString
public class MDBSQLDriver {

    @Getter
    private String url;
    @Getter
    private final String driverClass = "net.ucanaccess.jdbc.UcanaccessDriver";

    public void setUrl(String database) {
        url = "jdbc:ucanaccess://" + database + ";memory=true";
    }
}
