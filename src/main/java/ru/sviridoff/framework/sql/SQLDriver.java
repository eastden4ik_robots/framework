package ru.sviridoff.framework.sql;

import lombok.Getter;

import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.framework.sql.drivers.MDBSQLDriver;
import ru.sviridoff.framework.sql.drivers.MySQLDriver;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SQLDriver {

    // JDBC variables for opening and managing connection
    @Getter
    private Connection con;

    private static Logger log = new Logger("SQL");

    public void init(MySQLDriver config) {
        try {
            Class.forName(config.getDriverClass());
            con = DriverManager.getConnection(config.getUrl(), config.getUser(), config.getPassword());
        } catch (Exception sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    public void init(MDBSQLDriver config) {
        try {
            Class.forName(config.getDriverClass());
            con = DriverManager.getConnection(config.getUrl());
        } catch (Exception sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    public void updateQuery(String query) {
        try {
//            sql= "UPDATE USERS SET CONEXIONES = 1 WHERE USUARIO = ? AND CONTRASENA = ?";
            PreparedStatement pstmt = con.prepareStatement(query);
            pstmt.executeUpdate();
            log.info("Запрос [" + query + "] проведен.");
        } catch (SQLException ex) {
            log.error("Error: " + ex.getMessage());
        }
    }

    public void executeQuery(String query) {
        try {
            Statement statement = con.createStatement();
            statement.executeUpdate(query);
            log.info("Запрос [" + query + "] проведен.");
        } catch (SQLException ex) {
            log.error("Error: " + ex.getMessage());
        }
    }

    public List<Map<String, Object>> selectQuery(String query) {
        List<Map<String, Object>> result = new ArrayList<>();
        try {
            Statement stmt = con.createStatement();

            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Map<String, Object> res = new HashMap<>();
                for (int i = 1; i <= rs.getMetaData().getColumnCount() ; i++) {
                    res.put(rs.getMetaData().getColumnLabel(i), rs.getString(i));
                }
                result.add(res);
            }
        } catch (SQLException ex) {
            log.error(ex.getSQLState() + " -> " + ex.getMessage());
        }
        return result;
    }

    public void destruct() {
        try {
            con.close();
        } catch (Exception sqlEx) {
            sqlEx.printStackTrace();
        }
    }

}
