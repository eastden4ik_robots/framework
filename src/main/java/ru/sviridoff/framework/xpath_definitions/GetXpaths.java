package ru.sviridoff.framework.xpath_definitions;

import au.com.bytecode.opencsv.CSVReader;
import ru.sviridoff.framework.logger.Logger;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GetXpaths {

    private static CSVReader reader;
    private static Logger logger = new Logger(GetXpaths.class.getName());

    public GetXpaths(String fileName) {
        try {
            reader = new CSVReader(new FileReader(fileName), ';', '"', 1);
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
    }

    public List<XpathsModel> getXpaths(String pageName) {
        List<XpathsModel> xpathsModelList = new ArrayList<>();
        try {
            List<String[]> allRows = reader.readAll();
            for (String[] row: allRows) {
                if (row[0].toLowerCase().equals(pageName.toLowerCase())) {
                    XpathsModel xpathsModel = new XpathsModel();
                    xpathsModel.setPageName(row[0]);
                    xpathsModel.setFieldType(row[1]);
                    xpathsModel.setFieldName(row[2]);
                    xpathsModel.setFieldValue(row[3]);
                    xpathsModelList.add(xpathsModel);
                }
            }
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
        if (xpathsModelList.isEmpty()) {
            logger.error("Такой [" + pageName + "] страницы нет в списке.");
            throw new RuntimeException("Такой [" + pageName + "] страницы нет в списке.");
        }
        return xpathsModelList;
    }

}
