package ru.sviridoff.framework.xpath_definitions;

import java.util.ArrayList;
import java.util.List;

public class XpathUtils {

    private List<XpathsModel> xpathsModelList;

    public XpathUtils(List<XpathsModel> xpathsModels, String pageName) {
        List<XpathsModel> list = new ArrayList<>();
        for (XpathsModel xpathsModel: xpathsModels) {
            if (xpathsModel.getPageName().toLowerCase().equals(pageName.toLowerCase())) {
                list.add(xpathsModel);
            }
        }
        xpathsModelList = list;
    }

    public String getXpathByType(String name) {
        String result = null;
        for (XpathsModel xpathsModel: xpathsModelList) {
            if (xpathsModel.getFieldType().toLowerCase().equals(name.toLowerCase())) {
                result = xpathsModel.getFieldValue();
            }
        }
        if (result == null) {
            throw new RuntimeException("Такого xpath нет в списке: " + name);
        }
        return result;
    }

    public String getXpathByName(String name) {
        String result = null;
        for (XpathsModel xpathsModel: xpathsModelList) {
            if (xpathsModel.getFieldName().toLowerCase().equals(name.toLowerCase())) {
                result = xpathsModel.getFieldValue();
            }
        }
        if (result == null) {
            throw new RuntimeException("Такого xpath нет в списке: " + name);
        }
        return result;
    }

}
