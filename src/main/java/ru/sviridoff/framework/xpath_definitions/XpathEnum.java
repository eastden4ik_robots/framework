package ru.sviridoff.framework.xpath_definitions;

public enum  XpathEnum {
    StaticText, Button, Link, RegularInput, SelectInput, SelectOption
}
