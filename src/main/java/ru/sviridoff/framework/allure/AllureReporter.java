package ru.sviridoff.framework.allure;

import io.qameta.allure.Allure;
import io.qameta.allure.AllureLifecycle;
import io.qameta.allure.model.Label;
import io.qameta.allure.model.Status;
import io.qameta.allure.model.StatusDetails;
import io.qameta.allure.model.TestResult;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.util.Arrays;
import java.util.UUID;

public class AllureReporter {
    private static AllureReporter reporter = null;

    private final AllureLifecycle lifecycle;
    private final UUID robotRunUUID;
    private final TestResult robotResult;

    private AllureReporter() {
        lifecycle = Allure.getLifecycle();
        robotRunUUID = UUID.randomUUID();
        robotResult = new TestResult();
    }

    public static void setUp(String fullName, String shortName) {
        setUp(fullName, shortName, shortName, shortName, shortName);
    }

    public static void setUp(String fullName, String shortName, String feature, String story, String suite) {
        if (reporter == null) {
            reporter = new AllureReporter();

            reporter.robotResult
                    .setUuid(String.valueOf(reporter.robotRunUUID))
                    .setName(shortName)
                    .setFullName(fullName)
                    .setStatus(Status.PASSED)
                    .setLabels(Arrays.asList(
                            new Label().setName("feature").setValue(feature),
                            new Label().setName("story").setValue(story),
                            new Label().setName("suite").setValue(suite)
                    ))
                    .setStatusDetails(new StatusDetails());
            reporter.lifecycle.scheduleTestCase(reporter.robotResult);
            reporter.lifecycle.startTestCase(String.valueOf(reporter.robotRunUUID));
        }
    }

    public static void processException(Throwable e) {
        reporter.robotResult.setStatus(Status.FAILED)
                .setStatusDetails(reporter.robotResult.getStatusDetails()
                        .setMessage(e.getMessage())
                        .setTrace(ExceptionUtils.getStackTrace(e)));
    }

    public static void tearDown() {
        if (reporter != null) {
            reporter.lifecycle.stopTestCase(String.valueOf(reporter.robotRunUUID));
            reporter.lifecycle.writeTestCase(String.valueOf(reporter.robotRunUUID));
        }
    }
}