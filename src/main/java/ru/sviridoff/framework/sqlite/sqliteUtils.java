package ru.sviridoff.framework.sqlite;

import ru.sviridoff.framework.logger.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class sqliteUtils {

    private static Logger logger = new Logger();

    public void SqlQuery(Connection connection, String sql) {
        try {
            Statement stmt = connection.createStatement();
            stmt.execute(sql);
            logger.Logging(Logger.LogLevel.INFO, "SQL запрос [ " + sql + " ] выполнен успешно.");
        } catch (SQLException ex) {
            logger.Logging(Logger.LogLevel.DANGER, ex.getMessage());
        }
    }

    public ResultSet SelectSqlQuery(Connection connection, String sql) {
        ResultSet resultSet = null;
        try {
            Statement stmt = connection.createStatement();
            resultSet = stmt.executeQuery(sql);
            logger.Logging(Logger.LogLevel.INFO, "SQL запрос [ " + sql + " ] выполнен успешно.");
        } catch (SQLException ex) {
            logger.Logging(Logger.LogLevel.DANGER, ex.getMessage());
        }
        return resultSet;
    }

}
