package ru.sviridoff.framework.sqlite;

import ru.sviridoff.framework.logger.Logger;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;

public class initConnection {

    private static Logger logger = new Logger();
    private static Connection connection;

    public Connection getConnection() {
        return connection;
    }

    public void destroyConnection() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException ex) {
            logger.Logging(Logger.LogLevel.DANGER, ex.getMessage());
        }
    }

    public initConnection(String dbName) {
        Connection conn = null;
        try {

            Class.forName("org.sqlite.JDBC");
            String url = "jdbc:sqlite:" + dbName;
            conn = DriverManager.getConnection(url);

            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                logger.Logging(Logger.LogLevel.INFO, "The driver name is " + meta.getDriverName());
                logger.Logging(Logger.LogLevel.INFO, "A new database has been created.");
            }

            connection = conn;
            logger.Logging(Logger.LogLevel.INFO, "Connection to SQLite has been established.");

        } catch (SQLException | ClassNotFoundException e) {
            logger.Logging(Logger.LogLevel.DANGER, e.getMessage());
        }
    }

}
