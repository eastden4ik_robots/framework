package ru.sviridoff.framework.page_object_pattern;

import org.openqa.selenium.WebDriver;
import ru.sviridoff.framework.props.Parameters;
import ru.sviridoff.framework.xpath_definitions.GetXpaths;
import ru.sviridoff.framework.xpath_definitions.XpathsModel;

import java.util.List;

abstract public class AbstractPageObject {

    private String pageName;
    private WebDriver webDriver;
    private List<XpathsModel> xpathsModelList;

    public AbstractPageObject(String name, WebDriver driver) {
        pageName = name;
        webDriver = driver;
        Parameters parameters = new Parameters();
        GetXpaths getXpaths = new GetXpaths((String) parameters.getValue("xpaths.file"));
        xpathsModelList = getXpaths.getXpaths(pageName);
    }

    public List<XpathsModel> getXpathsModelList() {
        return xpathsModelList;
    }

    public String getPageName() {
        return pageName;
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }
}
