package ru.sviridoff.framework.props;

import lombok.Getter;
import ru.sviridoff.framework.logger.Logger;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Props {

    @Getter
    private Properties properties = new Properties();


    public Props(String properties) {
        Logger logger = new Logger(Props.class.getName());
        try {
            FileInputStream fis = new FileInputStream(properties);
            this.properties.load(fis);
            fis.close();
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
    }

    public String getProperty(String name) {
        return this.properties.getProperty(name);
    }

    public void setProperties(String name, String value) {
        this.properties.setProperty(name, value);
    }

    public void uploadProperties(String file) throws IOException {
        this.properties.store(new FileOutputStream(file), null);
    }

    @Override
    public String toString() {
        return "Props{\n" +
                "\tproperties=" + properties + "\n" +
                '}';
    }
}
