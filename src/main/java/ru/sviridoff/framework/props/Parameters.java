package ru.sviridoff.framework.props;

import java.util.Properties;

public class Parameters {

    private static Properties properties = null;

    public Parameters() {}

    public Parameters(String fileName) {

        Props props = new Props(fileName.contains(".")? fileName: fileName + ".properties");
        properties = props.getProperties();

    }

    public Object getValue(String name) {
        Object result;
        if (System.getProperty(name) != null) {
            result = System.getProperty(name);
        } else if (System.getenv(name) != null) {
            result = System.getenv(name);
        } else if (properties != null) {
            if (properties.getProperty(name) != null) {
                result = properties.getProperty(name);
            } else {
                throw new RuntimeException("Вы не задали данную переменную - [" + name + "].");
            }
        }
        else {
            throw new RuntimeException("Вы не задали данную переменную - [" + name + "].");
        }
        return result;
    }

}
