package ru.sviridoff.framework.logger;

import ru.sviridoff.framework.console.ConsoleColors;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

    private String LOG_NAME;
    private String LOG_FILENAME;
    private String PATTERN_DATE = "dd.MM.yyyy HH:mm:ss";
    private SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_DATE);
    private boolean txtLog;

    private boolean debugLog = false;

    public Logger() {
        this.LOG_NAME = "[Default logger]";
        this.txtLog = false;
    }

    public Logger(String logName) {
        this.LOG_NAME = "[" + logName + "]";
        this.txtLog = false;
    }

    public Logger(String logName, String fileName) {
        this.LOG_NAME = "[" + logName + "]";
        this.LOG_FILENAME = fileName;
        this.txtLog = true;
    }

    public void setDebugLevel(boolean level) {
        this.debugLog = level;
    }

    private void writeToFile(String message) throws IOException {
        FileWriter fw = new FileWriter(new File(this.LOG_FILENAME), true);
        fw.write(message);
        fw.write(System.getProperty("line.separator"));
        fw.close();
    }

    private String getMessage(LogLevel level, String message) {
        String result = "";
        switch (level) {
            case INFO: {
                result += (ConsoleColors.PURPLE_BRIGHT) + "[!] " + (ConsoleColors.RESET) + sdf.format(new Date()) + " : " + this.LOG_NAME + " : " + (ConsoleColors.PURPLE_BRIGHT) + message + ConsoleColors.RESET;
                break;
            }
            case SUCCESS: {
                result += (ConsoleColors.GREEN_BRIGHT) + "[+] " + (ConsoleColors.RESET) + sdf.format(new Date()) + " : " + this.LOG_NAME + " : " + (ConsoleColors.GREEN_BRIGHT) + message + ConsoleColors.RESET;
                break;
            }
            case WARNING: {
                result += (ConsoleColors.YELLOW_BRIGHT) + "[!] " + (ConsoleColors.RESET) + sdf.format(new Date()) + " : " + this.LOG_NAME + " : " + (ConsoleColors.YELLOW_BRIGHT) + message + ConsoleColors.RESET;
                break;
            }
            case DANGER: {
                result += (ConsoleColors.RED_BRIGHT) + "[-] " + (ConsoleColors.RESET) + sdf.format(new Date()) + " : " + this.LOG_NAME + " : " + (ConsoleColors.RED_BRIGHT) + message + ConsoleColors.RESET;
                break;
            }
            case STEPS: {
                result += (ConsoleColors.CYAN_BRIGHT) + "[=] " + (ConsoleColors.RESET) + sdf.format(new Date()) + " : " + this.LOG_NAME + " : " + (ConsoleColors.CYAN_BRIGHT) + message + ConsoleColors.RESET;
                break;
            }
            case DEBUG: {
                result += (ConsoleColors.YELLOW_BOLD) + "[D] " + (ConsoleColors.RESET) + sdf.format(new Date()) + " : " + this.LOG_NAME + " : " + (ConsoleColors.YELLOW_BOLD) + message + ConsoleColors.RESET;
                break;
            }
            default: {
                result += ConsoleColors.CYAN + "[UNDEFINED] " + ConsoleColors.RESET + sdf.format(new Date()) + " : " + this.LOG_NAME + " : " + ConsoleColors.CYAN + message + ConsoleColors.RESET;
                break;
            }
        }
        return result;
    }

    public void debug(String logMessage) {
        if (debugLog) {
            try {
                String message = getMessage(LogLevel.DEBUG, logMessage);
                System.out.println(message);

                if (txtLog) {
                    message = message.replaceAll("\\x1B(?:[@-Z\\\\-_]|\\[[0-?]*[ -/]*[@-~])", "");
                    writeToFile(message);
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void info(String logMessage) {
        try {
            String message = getMessage(LogLevel.INFO, logMessage);
            System.out.println(message);

            if (txtLog) {
                message = message.replaceAll("\\x1B(?:[@-Z\\\\-_]|\\[[0-?]*[ -/]*[@-~])", "");
                writeToFile(message);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void warning(String logMessage) {
        try {
            String message = getMessage(LogLevel.WARNING, logMessage);
            System.out.println(message);
            if (txtLog) {
                message = message.replaceAll("\\x1B(?:[@-Z\\\\-_]|\\[[0-?]*[ -/]*[@-~])", "");
                writeToFile(message);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void error(String logMessage) {
        try {
            String message = getMessage(LogLevel.DANGER, logMessage);
            System.out.println(message);
            if (txtLog) {
                message = message.replaceAll("\\x1B(?:[@-Z\\\\-_]|\\[[0-?]*[ -/]*[@-~])", "");
                writeToFile(message);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void success(String logMessage) {
        try {
            String message = getMessage(LogLevel.SUCCESS, logMessage);
            System.out.println(message);
            if (txtLog) {
                message = message.replaceAll("\\x1B(?:[@-Z\\\\-_]|\\[[0-?]*[ -/]*[@-~])", "");
                writeToFile(message);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void steps(String logMessage) {
        try {
            String message = getMessage(LogLevel.STEPS, logMessage);
            System.out.println(message);
            if (txtLog) {
                message = message.replaceAll("\\x1B(?:[@-Z\\\\-_]|\\[[0-?]*[ -/]*[@-~])", "");
                writeToFile(message);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @deprecated
     * */
    public void Logging(LogLevel level, String message) {

        SimpleDateFormat df = new SimpleDateFormat(PATTERN_DATE);
        switch (level) {
            case INFO: {
                System.out.println(ConsoleColors.BLUE_BOLD + "[ИНФО]" + ConsoleColors.RESET + " " + df.format(new Date()) + " : " + message);
                break;
            }
            case SUCCESS: {
                System.out.println(ConsoleColors.GREEN_BOLD + "[УСПЕХ]" + ConsoleColors.RESET + " " + df.format(new Date()) + " : " + message);
                break;
            }
            case WARNING: {
                System.out.println(ConsoleColors.YELLOW_BOLD + "[ПРЕДУПРЕЖДЕНИЕ]" + ConsoleColors.RESET  + " " + df.format(new Date()) + " : " + message);
                break;
            }
            case DANGER: {
                System.out.println(ConsoleColors.RED_BOLD + "[ОШИБКА]" + ConsoleColors.RESET  + " " + df.format(new Date()) + " : " + message);
                break;
            }
            case STEPS: {
                System.out.println(ConsoleColors.CYAN_BRIGHT + "[STEPS]" + ConsoleColors.RESET  + " " + df.format(new Date()) + " : " + message);
                break;
            }
            default: {
                System.out.println(ConsoleColors.RED_BOLD_BRIGHT + "[UNDEFINED]" + " " + df.format(new Date()) + " : " + message + ConsoleColors.RESET);
                break;
            }
        }
    }

    public enum LogLevel {
        INFO, SUCCESS, WARNING, DANGER, STEPS, DEBUG
    }

    /**
     * @deprecated
     * */
    public void Logging(LogLevel level, String message, String log) {
        try {
            Date now = new Date();
            SimpleDateFormat df = new SimpleDateFormat(" - [dd.MM.yyyy hh:mm:ss]: ");
            FileWriter fw = new FileWriter(new File(log), true);
            switch (level) {
                case INFO: {
                    System.out.println(ConsoleColors.BLUE_BOLD + "[ИНФО]" + ConsoleColors.RESET + " " + df.format(now) + " : " + message);
                    fw.write("[INFO]" + df.format(now) + message);
                    fw.write(System.getProperty("line.separator"));
                    break;
                }
                case SUCCESS: {
                    System.out.println(ConsoleColors.GREEN_BOLD + "[УСПЕХ]" + ConsoleColors.RESET + " " + df.format(now) + " : " + message);
                    fw.write("[SUCCESS]" + df.format(now) + message);
                    fw.write(System.getProperty("line.separator"));
                    break;
                }
                case WARNING: {
                    System.out.println(ConsoleColors.YELLOW_BOLD + "[ПРЕДУПРЕЖДЕНИЕ]" + ConsoleColors.RESET  + " " + df.format(now) + " : " + message);
                    fw.write("[WARNING]" + df.format(now) + message);
                    fw.write(System.getProperty("line.separator"));
                    break;
                }
                case DANGER: {
                    System.out.println(ConsoleColors.RED_BOLD + "[ОШИБКА]" + ConsoleColors.RESET  + " " + df.format(now) + " : " + message);
                    fw.write("[DANGER]" + df.format(now) + message);
                    fw.write(System.getProperty("line.separator"));
                    break;
                }
                case STEPS: {
                    System.out.println(ConsoleColors.CYAN_BRIGHT + "[STEPS]" + ConsoleColors.RESET  + " " + df.format(now) + " : " + message);
                    fw.write("[STEPS]" + df.format(now) + message);
                    fw.write(System.getProperty("line.separator"));
                    break;
                }
                default: {
                    System.out.println(ConsoleColors.RED_BOLD_BRIGHT + "[UNDEFINED]" + " " + df.format(now) + " : " + message + ConsoleColors.RESET);
                    break;
                }
            }
            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
