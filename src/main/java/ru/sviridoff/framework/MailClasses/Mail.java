package ru.sviridoff.framework.MailClasses;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.util.Properties;

public class Mail {

    private static final String ENCODING = "UTF-8";
    private String smtpHost;
    private String smtpPort;
    private Authenticator auth;
    private String addressFrom;

    public Mail(String smtpHost, String smtpPort, String login, String password, String addressFrom) {
        this.smtpHost = smtpHost;
        this.smtpPort = smtpPort;
        this.auth = new RobotMailAuthenticator(login, password);
        this.addressFrom = addressFrom;
    }

    public void sendMessage(String subject, String content, String addressTo) throws MessagingException {
        Properties props = System.getProperties();
        props.put("mail.smtp.host", smtpHost);
        props.put("mail.smtp.auth", "true");
        props.put("mail.mime.charset", ENCODING);
        props.put("mail.smtp.socketFactory.port", smtpPort);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        Session session = Session.getDefaultInstance(props, auth);

        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(addressFrom));
        msg.setRecipients(MimeMessage.RecipientType.TO, InternetAddress.parse(addressTo));
        msg.setSubject(subject);

        Multipart multipart = new MimeMultipart();
        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent(content, "text/html; charset=UTF-8");
        multipart.addBodyPart(mimeBodyPart);
        msg.setContent(multipart);
        Transport.send(msg);
    }

    public void sendMessageWithAttachment(String subject, String content, String addressTo, String filePath) throws MessagingException {
        Properties props = System.getProperties();
        props.put("mail.smtp.host", smtpHost);
        props.put("mail.smtp.auth", "true");
        props.put("mail.mime.charset", ENCODING);
        props.put("mail.smtp.socketFactory.port", smtpPort);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        Session session = Session.getDefaultInstance(props, auth);

        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(addressFrom));
        message.setRecipients(MimeMessage.RecipientType.TO, InternetAddress.parse(addressTo));
        message.setSubject(subject);
        message.setText(content);



        MimeBodyPart msg = new MimeBodyPart();
        Multipart multipart = new MimeMultipart();

        String fileName = new File(filePath).getName();
        FileDataSource source = new FileDataSource(filePath);
        msg.setDataHandler(new DataHandler(source));
        msg.setFileName(fileName);
        multipart.addBodyPart(msg);
        message.setContent(multipart);
        Transport.send(message);
    }


}
